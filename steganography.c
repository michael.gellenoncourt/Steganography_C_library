#include "steganography.h"

struct bitmap
{
    char *header;
    //file header
    uint16_t signature;
    uint32_t fileSize;
    uint32_t pixelArrayOffset;

    //bitmap header
    uint32_t headerSize;
    int32_t width;
    int32_t height;
    uint16_t planes;
    uint16_t bitsPerPixel;
    uint32_t compression;
    uint32_t imageSize;
    int32_t xPixelPerMeter;
    int32_t yPixelPerMeter;
    uint32_t colorsNumber;
    uint32_t importantColorCount;
    uint32_t redMask;
    uint32_t greenMask;
    uint32_t blueMask;
    uint32_t alphaMask;
    uint32_t csType;
    uint32_t redGamma;
    uint32_t greenGamma;
    uint32_t blueGamma;
    uint32_t alphaGamma;
    uint32_t intent;
    uint32_t profileData;
    uint32_t profileDataSize;

    //pixels
    pixel **image;
};
struct pixel
{
    uint16_t red;
    uint16_t blue;
    uint16_t green;
    uint16_t alpha;
};
static int loadBMP(char *filePath,bitmap *bmp)
{
    if(filePath == NULL)
    {
        return NULL_FILE_PATH_ERROR;
    }
    pathToOSFormat(filePath);
    FILE *file;
    if(file = fopen(filePath,"rb") == NULL)
    {
        return FILE_DOES_NOT_EXIST_ERROR;
    }

    fread(bmp->signature,sizeof(bmp->signature),1,file);

    switch(bmp->signature)
    {
        case WINDOWS_BITMAP:

            fread(bmp->fileSize,sizeof(bmp->fileSize),1,file);
            fseek(file,sizeof(uint32_t),SEEK_CUR);
            fread(bmp->pixelArrayOffset,sizeof(bmp->pixelArrayOffset),1,file);

            fread(bmp->headerSize,sizeof(bmp->headerSize),1,file);
            fread(bmp->width,sizeof(bmp->width),1,file);
            fread(bmp->height,sizeof(bmp->height),1,file);
            fseek(file,sizeof(uint16_t),SEEK_CUR); //skip planes
            fread(bmp->bitsPerPixel,sizeof(bmp->bitsPerPixel),1,file);
            fseek(file,sizeof(uint32_t),SEEK_CUR); //skip compression
            fread(bmp->imageSize,sizeof(bmp->imageSize),1,file);
            fseek(file,sizeof(int32_t)*2,SEEK_CUR); //Skip pixels per meter
            fseek(file,sizeof(uint32_t),SEEK_CUR); //skip number of colors in color array
            fseek(file,sizeof(uint32_t),SEEK_CUR); //skip number of important colors in color array

            if(bmp->headerSize == BITMAPV4HEADER || bmp->headerSize == BITMAPV5HEADER)
            {
                fread(bmp->redMask,sizeof(bmp->redMask),1,file);
                fread(bmp->greenMask,sizeof(bmp->redMask),1,file);
                fread(bmp->blueMask,sizeof(bmp->redMask),1,file);
            }

            fseek(file,bmp->pixelArrayOffset,SEEK_SET);

            if(bmp->headerSize == BITMAPINFOHEADER)
            {
                //todo
            }
            else
            {
                //todo
            }

        break;
        default:
            return FORMAT_NOT_SUPPORTED_ERROR;
        break;
    }

    close(file);

}
static void pathToOSFormat(char *filePath)
{
    char *p = strchr(filePath,INVALID_FS);
    while(p != NULL)
    {
        *p = FS;
        p = strchr(filePath,INVALID_FS);
    }
}
