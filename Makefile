CC=gcc
CFLAGS=-Wall -g
LDFLAGS=

EXECS=steganograph

all: $(EXECS)

steganograph: steganograph.o bitmap.o
	$(CC) $(LDFLAGS) -o $@ $^

steganograph.o: bitmap.h

steganograph.a: steganograph.o bitmap.o
	ar rcs $@ $^

%.o: %.c %.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f *.o

mrproper:
	rm -f $(EXECS)

run_%: %
	./$<