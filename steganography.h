#ifndef STEGANOGRAPHY_H_INCLUDED
#define STEGANOGRAPHY_H_INCLUDED

#include <string.h>
#include <stat.h>

#define WINDOWS_BITMAP 0x4D42
#define BITMAPINFOHEADER 40
#define BITMAPV4HEADER 108
#define BITMAPV5HEADER 124


#ifdef WIN32
    #define FS '\\'
    #define INVALID_FS '/'
#else
    #define FS '/'
    #define INVALID_FS '\\'
#endif

typedef struct bitmap bitmap;
typedef struct pixel pixel;

enum {NO_ERROR,NULL_FILE_PATH_ERROR,FILE_DOES_NOT_EXIST_ERROR,FORMAT_NOT_SUPPORTED_ERROR};

static int loadBMP(char *filePath,bitmap *bmp);
static void pathToOSFormat(char *filePath);


#endif // STEGANOGRAPHY_H_INCLUDED
